# Load Balancer Solution #


### Description ###
A simple Load Balancer simulator

This solution can:

* Generate a provider
* Register a list of providers
* Perform Random invocation of a provider 
* Perform Round Robin invocation of providers
* Allow Manual provider node exclusion / inclusion
* Allow Heart beat check for alive providers to exclude on re-include the nodes from the cluster 
* Manage cluster capacity

## Testing ###

The following test cases are covered in the `LoadBalancerTest.java` class under the test package `com.iptiq.loadbalancer`:

* testProvidersAreRegistered()
* testProviderReturnsUniqueIdentifier()
* testAllProvidersIdentifiersAreUnique() 
* testRandomLoadBalancerInvocation()
* testRoundRobinLoadBalancerInvocation()
* testExcludeProvider()
* testExcludeNonExistentProvider()
* testIncludeNonExistentProvider()
* testIncludeNotExcludedProvider()
* testIncludeProvider()
* testHeartBeatCheck()
* testImprovedHeartBeatCheck()
* testMaximumClusterCapacityForSimultaneousRequests()
* testMaximumClusterCapacityForSimultaneousRequestsWhenSomeNodesNotAlive()
