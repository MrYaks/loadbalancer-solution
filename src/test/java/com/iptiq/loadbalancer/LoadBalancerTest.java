package com.iptiq.loadbalancer;

import com.iptiq.loadbalancer.balancer.LoadBalancer;
import com.iptiq.loadbalancer.balancer.RandomLoadBalancer;
import com.iptiq.loadbalancer.balancer.RoundRobinLoadBalancer;
import com.iptiq.loadbalancer.delegate.LoadBalancerDelegate;
import com.iptiq.loadbalancer.provider.Provider;
import org.junit.Test;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

/**
 * Unit test for LoadBalancer.
 */
public class LoadBalancerTest {
    private final static int HEART_BEAT_FREQUENCY_IN_SECONDS = 2;
    private final static int IMPROVED_HEART_BEAT_FREQUENCY_IN_SECONDS = 5;
    private final static int MAX_NO_OF_PARALLEL_REQUESTS_PER_PROVIDER = 3;
    private final static int NUM_OF_REQUESTS_TO_LOADBALANCER = 15;
    private final static int MAX_NUMBER_OF_PROVIDERS = 10;
    LoadBalancerDelegate loadBalancerDelegate = new LoadBalancerDelegate(MAX_NUMBER_OF_PROVIDERS);

    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }

    @Test
    public void testProvidersAreRegistered() {
        assertTrue(loadBalancerDelegate.getAllProviders() != null && loadBalancerDelegate.getProviders().size() == MAX_NUMBER_OF_PROVIDERS);
    }

    @Test
    public void testProviderReturnsUniqueIdentifier() {
        assertNotNull(loadBalancerDelegate.getAllProviders().get(0).getUniqueIdentifier());
    }

    @Test
    public void testAllProvidersIdentifiersAreUnique() {
        List<String> identifierList = loadBalancerDelegate.getAllProviders().stream().map(Provider::getUniqueIdentifier).collect(Collectors.toList());
        Set<String> identifierSet = new HashSet<>(identifierList);
        assertTrue(identifierSet.size() == identifierList.size());
    }

    @Test
    public void testRandomLoadBalancerInvocation() {
        LoadBalancer loadBalancer = new RandomLoadBalancer(loadBalancerDelegate, MAX_NO_OF_PARALLEL_REQUESTS_PER_PROVIDER);
        printInvocation("Random");
        simulateClientRequest(loadBalancer, NUM_OF_REQUESTS_TO_LOADBALANCER);
    }

    @Test
    public void testRoundRobinLoadBalancerInvocation() {
        LoadBalancer loadBalancer = new RoundRobinLoadBalancer(loadBalancerDelegate, MAX_NO_OF_PARALLEL_REQUESTS_PER_PROVIDER);
        printInvocation("Round-Robin");
        simulateClientRequest(loadBalancer, NUM_OF_REQUESTS_TO_LOADBALANCER);
    }

    @Test
    public void testExcludeProvider() {
        LoadBalancer loadBalancer = new RandomLoadBalancer(loadBalancerDelegate, MAX_NO_OF_PARALLEL_REQUESTS_PER_PROVIDER);
        String providerToExclude = loadBalancerDelegate.getProviders().get(loadBalancerDelegate.getProviders().size() - 1).getUniqueIdentifier();
        loadBalancer.excludeProvider(providerToExclude);
        assertTrue(loadBalancerDelegate.getProviders().stream().noneMatch(provider -> provider.getUniqueIdentifier().equals(providerToExclude)));
    }

    @Test
    public void testExcludeNonExistentProvider() {
        LoadBalancer loadBalancer = new RandomLoadBalancer(loadBalancerDelegate, MAX_NO_OF_PARALLEL_REQUESTS_PER_PROVIDER);
        assertEquals("Provider with identifier does not exist or is already excluded from the LoadBalancer.", loadBalancer.excludeProvider(UUID.randomUUID().toString()));
    }

    @Test
    public void testIncludeNonExistentProvider() {
        LoadBalancer loadBalancer = new RandomLoadBalancer(loadBalancerDelegate, MAX_NO_OF_PARALLEL_REQUESTS_PER_PROVIDER);
        String providerToInclude = UUID.randomUUID().toString();
        assertEquals("Provider with identifier " + providerToInclude + " does not exist on the LoadBalancer.", loadBalancer.includeProvider(providerToInclude));
    }

    @Test
    public void testIncludeNotExcludedProvider() {
        LoadBalancer loadBalancer = new RandomLoadBalancer(loadBalancerDelegate, MAX_NO_OF_PARALLEL_REQUESTS_PER_PROVIDER);
        String providerToInclude = loadBalancerDelegate.getAllProviders().get(0).getUniqueIdentifier();
        assertEquals("Provider has to be excluded from the LoadBalancer, before calling include.", loadBalancer.includeProvider(providerToInclude));
    }

    @Test
    public void testIncludeProvider() {
        LoadBalancer loadBalancer = new RandomLoadBalancer(loadBalancerDelegate, MAX_NO_OF_PARALLEL_REQUESTS_PER_PROVIDER);
        String providerToExcludeAndInclude = loadBalancerDelegate.getProviders().get(loadBalancerDelegate.getProviders().size() - 1).getUniqueIdentifier();
        loadBalancer.excludeProvider(providerToExcludeAndInclude); // Exclude the provider first
        loadBalancer.includeProvider(providerToExcludeAndInclude);
        assertTrue(loadBalancerDelegate.getProviders().stream().anyMatch(provider -> provider.getUniqueIdentifier().equals(providerToExcludeAndInclude)));
    }

    @Test
    public void testHeartBeatCheck() throws InterruptedException {
        LoadBalancer loadBalancer = new RandomLoadBalancer(loadBalancerDelegate, MAX_NO_OF_PARALLEL_REQUESTS_PER_PROVIDER);
        Provider provider1 = loadBalancerDelegate.getProviders().get(0);
        // Set providers active to false to simulate provider not alive
        provider1.setActive(false);
        Provider provider2 = loadBalancerDelegate.getProviders().get(1);
        provider2.setActive(false);
        loadBalancerDelegate.updateProvider(provider1);
        loadBalancerDelegate.updateProvider(provider2);
        loadBalancer.heartBeatCheck(HEART_BEAT_FREQUENCY_IN_SECONDS);
        Thread.sleep(5000);// 5 seconds should be sufficient to demonstrate 2 executions (every 2 minutes).
        //Test that both providers have been excluded since health check failed.
        assertTrue(loadBalancerDelegate.getProviders().stream().noneMatch((provider -> provider.getUniqueIdentifier().equals(provider1.getUniqueIdentifier()))));
        assertTrue(loadBalancerDelegate.getProviders().stream().noneMatch(provider -> provider.getUniqueIdentifier().equals(provider2.getUniqueIdentifier())));
    }

    @Test
    public void testImprovedHeartBeatCheck() throws InterruptedException {
        LoadBalancer loadBalancer = new RandomLoadBalancer(loadBalancerDelegate, MAX_NO_OF_PARALLEL_REQUESTS_PER_PROVIDER);
        Provider provider1 = loadBalancerDelegate.getProviders().get(0);
        // Set providers active to false to simulate provider not alive
        provider1.setActive(false);
        Provider provider2 = loadBalancerDelegate.getProviders().get(1);
        provider2.setActive(false);
        loadBalancerDelegate.updateProvider(provider1);
        loadBalancerDelegate.updateProvider(provider2);
        loadBalancer.heartBeatCheck(HEART_BEAT_FREQUENCY_IN_SECONDS);
        Thread.sleep(2000);// 5 seconds should be sufficient for 2 executions (every 2 minutes).
        //Simulate both providers now healthy before first improved check
        provider1.setActive(true);
        provider2.setActive(true);
        loadBalancerDelegate.updateProvider(provider1);
        loadBalancerDelegate.updateProvider(provider2);
        loadBalancer.improvedHeartBeatCheck(IMPROVED_HEART_BEAT_FREQUENCY_IN_SECONDS);
        Thread.sleep(2000); // 5 seconds should be sufficient for 1 execution
        //Simulate only second provider now healthy before second CONSECUTIVE improved check
        provider1.setActive(false);
        provider2.setActive(true);
        loadBalancerDelegate.updateProvider(provider1);
        loadBalancerDelegate.updateProvider(provider2);
        loadBalancer.improvedHeartBeatCheck(IMPROVED_HEART_BEAT_FREQUENCY_IN_SECONDS);
        Thread.sleep(2000); // 5 seconds should be sufficient for 1 execution
        //Test that only the second provider has been re included after 2 CONSECUTIVE successful heart beat checks
        assertTrue(loadBalancerDelegate.getProviders().stream().noneMatch((provider -> provider.getUniqueIdentifier().equals(provider1.getUniqueIdentifier()))));
        assertTrue(loadBalancerDelegate.getProviders().stream().anyMatch(provider -> provider.getUniqueIdentifier().equals(provider2.getUniqueIdentifier())));
    }

    @Test
    public void testMaximumClusterCapacityForSimultaneousRequests() throws InterruptedException {
        LoadBalancer loadBalancer = new RandomLoadBalancer(loadBalancerDelegate, MAX_NO_OF_PARALLEL_REQUESTS_PER_PROVIDER);
        int overLoadedNumberOfCalls = (MAX_NUMBER_OF_PROVIDERS * MAX_NO_OF_PARALLEL_REQUESTS_PER_PROVIDER) + 5;// test by adding extra simultaneous request
        ExecutorService executor = Executors.newFixedThreadPool(overLoadedNumberOfCalls);
        IntStream.range(0, overLoadedNumberOfCalls)
                .forEach(i ->
                        executor.submit(() -> {
                            System.out.println(
                                    "UniqueIdentifier: " + loadBalancer.getUniqueIdentifier());
                        })
                );
        Thread.sleep(1000);
        executor.shutdown();
    }

    @Test
    public void testMaximumClusterCapacityForSimultaneousRequestsWhenSomeNodesNotAlive() throws InterruptedException {
        Provider provider1 = loadBalancerDelegate.getProviders().get(0);
        // Set providers active to false to simulate provider not alive
        provider1.setActive(false);
        Provider provider2 = loadBalancerDelegate.getProviders().get(1);
        provider2.setActive(false);
        loadBalancerDelegate.updateProvider(provider1);
        loadBalancerDelegate.updateProvider(provider2);
        LoadBalancer loadBalancer = new RandomLoadBalancer(loadBalancerDelegate, MAX_NO_OF_PARALLEL_REQUESTS_PER_PROVIDER);
        int overLoadedNumberOfCalls = (MAX_NUMBER_OF_PROVIDERS * MAX_NO_OF_PARALLEL_REQUESTS_PER_PROVIDER) + 5;// test by adding extra simultaneous request
        ExecutorService executor = Executors.newFixedThreadPool(overLoadedNumberOfCalls);
        IntStream.range(0, overLoadedNumberOfCalls)
                .forEach(i ->
                        executor.submit(() -> {
                            System.out.println(
                                    "UniqueIdentifier: " + loadBalancer.getUniqueIdentifier());
                        })
                );
        Thread.sleep(1000);
        executor.shutdown();
    }


    private void simulateClientRequest(LoadBalancer loadBalancer, int numOfCalls) {
        IntStream
                .range(0, numOfCalls)
                .parallel()
                .forEach(i ->
                        System.out.println(
                                "UniqueIdentifier: " + loadBalancer.getUniqueIdentifier())
                );
    }

    private void printInvocation(String name) {
        System.out.println("---");
        System.out.println(name + " Load Balancer Invocation Test");
        System.out.println("---");
    }

    /**
     * Just on the side, solution for the Hackerank recursion exercise I didn't submit in time:-)
     */
    private static List<Integer> Odd(int L, int R, List<Integer> resultArr) {
        if (R < L) {
            return resultArr;
        }

        if (R % 2 == 1) {
            Odd(L, R - 2, resultArr);
        } else
            Odd(L, R - 1, resultArr);

        if (R % 2 == 1) {
            resultArr.add(R);
        }
        return resultArr;
    }

}
