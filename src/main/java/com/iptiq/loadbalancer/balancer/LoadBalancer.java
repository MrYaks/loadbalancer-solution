package com.iptiq.loadbalancer.balancer;

import com.iptiq.loadbalancer.delegate.LoadBalancerDelegate;
import com.iptiq.loadbalancer.provider.Provider;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public abstract class LoadBalancer {
    protected LoadBalancerDelegate loadBalancerDelegate;
    Map<String, Integer> excludedProviderMap = new HashMap<>();

    public abstract String getUniqueIdentifier();

    public abstract void updateProviderList();

    public String excludeProvider(String uniqueIdentifier) {
        List<Provider> providers = loadBalancerDelegate.getProviders();
        if (providers.stream().noneMatch(provider -> provider.getUniqueIdentifier().equals(uniqueIdentifier))) {
            System.out.println("Provider with identifier does not exist or is already excluded from the LoadBalancer.");
            return "Provider with identifier does not exist or is already excluded from the LoadBalancer.";
        }
        excludedProviderMap.put(uniqueIdentifier, 0);
        Provider providerToExclude = providers.stream()
                .filter(provider -> provider.getUniqueIdentifier().equals(uniqueIdentifier))
                .findFirst()
                .get();
        providerToExclude.setExcluded(true);
        loadBalancerDelegate.updateProvider(providerToExclude);
        System.out.println("Provider with identifier " + uniqueIdentifier + " excluded from the LoadBalancer.");
        return "Provider with identifier " + uniqueIdentifier + " excluded from the LoadBalancer.";
    }

    public String includeProvider(String uniqueIdentifier) {
        List<Provider> providers = loadBalancerDelegate.getAllProviders();
        if (providers.stream().noneMatch(provider -> provider.getUniqueIdentifier().equals(uniqueIdentifier))) {
            System.out.println("Provider with identifier " + uniqueIdentifier + " does not exist on the LoadBalancer.");
            return "Provider with identifier " + uniqueIdentifier + " does not exist on the LoadBalancer.";
        }

        if (Objects.isNull(excludedProviderMap.get(uniqueIdentifier))) {
            System.out.println("Provider has to be excluded from the LoadBalancer, before calling include.");
            return "Provider has to be excluded from the LoadBalancer, before calling include.";
        }

        excludedProviderMap.remove(uniqueIdentifier);
        Provider providerToInclude = providers.stream()
                .filter(provider -> provider.getUniqueIdentifier().equals(uniqueIdentifier))
                .findFirst()
                .get();
        providerToInclude.setExcluded(false);
        loadBalancerDelegate.updateProvider(providerToInclude);
        System.out.println("Provider with identifier " + uniqueIdentifier + " included to the LoadBalancer.");
        return "Provider with identifier " + uniqueIdentifier + " included to the LoadBalancer.";
    }

    public void heartBeatCheck(int frequencyInSecond) {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        Runnable task = () -> {
            System.out.println("Scheduling heartBeatCheck at: " + System.nanoTime());
            loadBalancerDelegate.getProviders().stream()
                    .filter(provider -> !provider.check())
                    .map(provider -> provider.getUniqueIdentifier())
                    .forEach(this::excludeProvider);
        };
        executor.scheduleWithFixedDelay(task, 0, frequencyInSecond, TimeUnit.SECONDS);
    }

    public void improvedHeartBeatCheck(int frequencyInSecond) {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        Runnable task = () -> {
            System.out.println("Scheduling improvedHeartBeatCheck at: " + System.nanoTime());
            excludedProviderMap.forEach((providerUniqueIdentifier, providerHealthCheckCount) -> {
                boolean providerNowHealthy = loadBalancerDelegate.getAllProviders().stream()
                        .filter(provider -> provider.getUniqueIdentifier().equals(providerUniqueIdentifier))
                        .map(Provider::check)
                        .findFirst()
                        .orElse(false);
                if (providerNowHealthy) {
                    switch (providerHealthCheckCount) {
                        // Denote first successful heartbeat
                        case 0:
                            excludedProviderMap.put(providerUniqueIdentifier, 1);
                            break;
                        default:
                            includeProvider(providerUniqueIdentifier);
                            excludedProviderMap.remove(providerUniqueIdentifier);
                    }
                } else
                    excludedProviderMap.put(providerUniqueIdentifier, 0);
            });
        };
        executor.scheduleWithFixedDelay(task, 0, frequencyInSecond, TimeUnit.SECONDS);
    }

    public void simulateProviderProcessingTime(int processingTime) {
        try {
            TimeUnit.MILLISECONDS.sleep(processingTime);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }

}
