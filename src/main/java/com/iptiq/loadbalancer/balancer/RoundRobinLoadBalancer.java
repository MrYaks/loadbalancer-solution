package com.iptiq.loadbalancer.balancer;

import com.iptiq.loadbalancer.delegate.LoadBalancerDelegate;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class RoundRobinLoadBalancer extends LoadBalancer {

    private int counter = 0;
    private final ReentrantLock lock;
    private final Semaphore semaphore;

    public RoundRobinLoadBalancer(LoadBalancerDelegate loadBalancerDelegate, int maxNumberOfParallelRequestPerProvider) {
        this.loadBalancerDelegate = loadBalancerDelegate;
        this.loadBalancerDelegate.attachLoadBalancer(this);
        semaphore = new Semaphore(maxNumberOfParallelRequestPerProvider * loadBalancerDelegate.aliveProvidersCount(), true);
        lock = new ReentrantLock();
        System.out.println("No of alive providers at init: " + semaphore.availablePermits());
    }

    @Override
    public String getUniqueIdentifier() {
        boolean permit = false;
        try {
            permit = semaphore.tryAcquire(1, TimeUnit.MILLISECONDS);
            if (permit) {
                //Simulate Provider Processing time
                simulateProviderProcessingTime(500);
                return getRoundRobinUniqueIdentifier();
            } else {
                return "Load balancer capacity exhausted. Cannot acquire new uniqueIdentifier.";
            }
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        } finally {
            if (permit) {
                semaphore.release();
            }
        }
    }

    public String getRoundRobinUniqueIdentifier() {
        lock.lock();
        try {
            String uniqueIdentifier = loadBalancerDelegate.getProviders()
                    .get(counter)
                    .getUniqueIdentifier();
            counter += 1;
            if (counter == loadBalancerDelegate.getProviders().size()) {
                counter = 0;
            }
            return uniqueIdentifier;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void updateProviderList() {
        System.out.println("RoundRobinLoadBalancer: provider list updated.");
    }

}
