package com.iptiq.loadbalancer.balancer;

import com.iptiq.loadbalancer.delegate.LoadBalancerDelegate;
import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class RandomLoadBalancer extends LoadBalancer {

    private final Semaphore semaphore;

    public RandomLoadBalancer(LoadBalancerDelegate loadBalancerDelegate, int maxNumberOfParallelRequestPerProvider) {
        this.loadBalancerDelegate = loadBalancerDelegate;
        this.loadBalancerDelegate.attachLoadBalancer(this);
        semaphore = new Semaphore(maxNumberOfParallelRequestPerProvider * loadBalancerDelegate.aliveProvidersCount(), true);
        System.out.println("No of alive providers at init: " + semaphore.availablePermits());

    }

    @Override
    public String getUniqueIdentifier() {
        boolean permit = false;
        try {
            permit = semaphore.tryAcquire(1, TimeUnit.MILLISECONDS);
            if (permit) {
                //Simulate Provider Processing time
                simulateProviderProcessingTime(500);
                return getRandomUniqueIdentifier();
            } else {
                return "Load balancer capacity exhausted. Cannot acquire new uniqueIdentifier.";
            }
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        } finally {
            if (permit) {
                semaphore.release();
            }
        }
    }

    private String getRandomUniqueIdentifier() {
        Random random = new Random();
        return loadBalancerDelegate.getProviders()
                .get(random.nextInt(loadBalancerDelegate.getProviders().size()))
                .getUniqueIdentifier();
    }

    @Override
    public void updateProviderList() {
        System.out.println("RandomLoadBalancer: provider list updated.");
    }

}
