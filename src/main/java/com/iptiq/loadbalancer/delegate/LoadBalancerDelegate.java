package com.iptiq.loadbalancer.delegate;

import com.iptiq.loadbalancer.balancer.LoadBalancer;
import com.iptiq.loadbalancer.provider.Provider;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LoadBalancerDelegate {

    private List<LoadBalancer> loadBalancers = new ArrayList<>();
    private List<Provider> providers = new ArrayList<>();

    public LoadBalancerDelegate(int maxNumberOfProviders) {
        for (int i = 0; i < maxNumberOfProviders; i++) {
            providers.add(new Provider("0.0.0." + (i + 1)));
        }
    }

    public List<Provider> getAllProviders() {
        return providers;
    }

    public List<Provider> getProviders() {
        return providers.stream()
                .filter(provider -> !provider.isExcluded())
                .collect(Collectors.toList());
    }

    public synchronized void updateProvider(Provider provider) {
        providers.remove(provider);
        providers.add(provider);
        notifyAllLoadBalancers();
    }

    public void attachLoadBalancer(LoadBalancer loadBalancer) {
        loadBalancers.add(loadBalancer);
    }

    public void notifyAllLoadBalancers() {
        for (LoadBalancer loadBalancer : loadBalancers) {
            loadBalancer.updateProviderList();
        }
    }

    public List<Provider> getAliveProviders() {
        return getProviders().stream()
                .filter(provider -> provider.check())
                .collect(Collectors.toList());
    }

    public int aliveProvidersCount() {
        return getAliveProviders().size();
    }

}
