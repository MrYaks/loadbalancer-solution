package com.iptiq.loadbalancer.provider;

public class Provider {

    public Provider(String uniqueIdentifier) {
        this.uniqueIdentifier = uniqueIdentifier;
    }
    
    private final String uniqueIdentifier;

    private boolean active = true;

    private boolean excluded = false;

    public boolean isExcluded() {
        return excluded;
    }

    public void setExcluded(boolean excluded) {
        this.excluded = excluded;
    }

    public String getUniqueIdentifier() {
        return uniqueIdentifier;
    }

    public boolean check() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }


}
